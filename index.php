<?php

require("./vendor/autoload.php");

// on reporte toutes les erreurs
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// on definit des constantes pour appeller les scripts qqsoit le directory
define('WEBROOT', str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT', str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));

// on recupere tous les paramètres de l'url séparés (ex: localhost/tuto/index)
$param = explode('/',$_GET["p"]);
$controller = $param[0] ; // tuto Objet
$controllerNamespace = "Game\\Controllers\\Impl\\" . $param[0] ; // tuto Objet
$method = $param[1] ; // index function de l'Objet


// on vérifie bien que la methode appellée existe dans la classe
if (method_exists($controllerNamespace, $method)) {
    $instance = new $controllerNamespace();

    switch($controller) {
        case "Character":
            switch($method) {
                case "create":
                    $instance->$method($param[2], $param[3], $param[4]);
                case "getAll":
                    $instance->$method();
                break;
            }
        case "Game":
            switch($method) {
                case "launch":
                    $instance->$method();
                break;
            }
        break;
    }

    
} else {
    echo "methode non existante, erreur 404";
}



