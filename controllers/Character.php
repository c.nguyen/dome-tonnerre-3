<?php

namespace Game\Controllers\Impl;

use Beweb\Td\Dal\DAOCharacter;
use Game\Controllers\Core\Controller;

class Character extends Controller {
    /**
     * Create a character
     *
     * @param String $name
     * @param String $race
     * @param String $job
     * @return void
     */
    public function create(String $name, String $race, String $job): void {
        //require(ROOT . "dome/src/Dal/DAOCharacter.php");
        $dao = new DAOCharacter();
        $dao->persist($dao->createCharacter($name, $race, $job));

        echo "Personnage : " . $name . "(" . $race . ", " . $job . ") a été créé !<br>";

    }

    /**
     * Show all characters who have been created
     *
     * @return array
     */
    public function getAll(): void {
        $dao = new DAOCharacter();
        
        $data["characters"] = $dao->load();
        $this->set($data);
        $this->render("characters");

    }

}