<?php

namespace Game\Controllers\Core;

class Controller {
    
    var $vars = array();

    /**
     * set data retrieve from the controller to render
     *
     * @param array $data - key value array
     * @return void
     */
    public function set($data): void {
        $this->vars = array_merge($this->vars , $data);
    }

    /**
     * rend a view with data
     *
     * @param String $filename - view to fill with content
     * @return void
     */
    public function render($filename): void{
        extract($this->vars);
        require(ROOT.'views/'.$filename.'.php');
    }


}


